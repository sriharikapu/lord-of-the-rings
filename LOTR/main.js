#!/usr/bin/env node
const bls = require('bls-lib')
const HDWalletProvider = require("truffle-hdwallet-provider")
const contract = require("truffle-contract");
const LotrJson = require('./build/contracts/Lotr.json')
const SHA3 = require('sha3')

function getNmemonic() {
  try{
    return require('fs').readFileSync("./seed", "utf8").trim()
  } catch(err){
    return "";
  }
}

let provider
function getProvider(rpcUrl) {
  if (!provider) {
    provider = new HDWalletProvider(getNmemonic(), rpcUrl, address_index=0, num_addresses=4)
  }
  return provider
}

function verifyContributionShare(bls, point, skcontribution, verificationVector) {
  const pk1 = bls.publicKey()
  bls.publicKeyShare(pk1, verificationVector, point)

  const pk2 = bls.publicKey()
  bls.getPublicKey(pk2, skcontribution)

  const isEqual = bls.publicKeyIsEqual(pk1, pk2)
  bls.free(pk1)
  bls.free(pk2)
  return Boolean(isEqual)
}

var Lotr = contract(LotrJson);
//Lotr.setProvider(getProvider("https://rinkeby.infura.io/") );
Lotr.setProvider(getProvider("http://localhost:8545") );
let addresses = provider.getAddresses()

bls.onModuleInit(() => {
  bls.init()
  console.log(addresses)

  Lotr
    .deployed()
    .then(async (instance) => {
       console.log("Let's start stuff...")

       const eval_points = []
       const threshold = await instance.threshold()
       const numMembers = await instance.numMembers()

       for(var i = 0; i < numMembers; i++) {
           eval_points.push(await instance.polynomialEvalPoints(i))
           console.log(".")
       }
       console.log("Eval points: " + eval_points)

       const members = eval_points.map(point => {
         const sk = bls.secretKey()
         bls.hashToSecretKey(sk, Buffer.from([point]))
         return {
           point: sk,
           address: "",
           receivedShares: Array(numMembers).fill(0)
         }
       })

       var points_sks = members.map(m => m.point)
       console.log('Beginning the secret instantiation round...')

       const svecs = Array(numMembers).fill(0)
       for(var m = 0; m < numMembers; m++) {
         var verificationVector = []
         var secretKeyContribution = []
         const svec = []

         for (let i = 0; i < threshold; i++) {
           const sk = bls.secretKey()
           bls.secretKeySetByCSPRNG(sk)
           svec.push(sk)
       
           const pk = bls.publicKey()
           bls.getPublicKey(pk, sk)
           verificationVector.push(parseInt(pk))
         }
       
         console.log(verificationVector)
         let tx = await instance.joinGroupSignature(
           verificationVector,
           { "from": addresses[m] },
         )

         for (const point_sk of points_sks) {
           const sk = bls.secretKey()
           bls.secretKeyShare(sk, svec, point_sk)
           secretKeyContribution.push(sk)
         }
         svec.forEach(s => bls.free(s))

         let my_id = (await instance.getMemberID({"from": addresses[m]})).toNumber()
         members[my_id].address = addresses[m]
         for(var mxx = 0; mxx < numMembers; mxx++) {
             members[mxx].receivedShares[my_id] = secretKeyContribution[mxx]
         }
         svecs[my_id] = secretKeyContribution
         console.log(my_id)
       }

       // Let's commit our secretKeyContributions hashed for each participant
       console.log("Let's commit hashes")
       for(var m = 0; m < numMembers; m++) {
         let sk_hashes = "";
         secretKeyContribution = svecs[m]
         for(var mxx = 0; mxx < numMembers; mxx++) {
             let d = new SHA3.SHA3Hash()
             d.update(secretKeyContribution[mxx].toString())
             sk_hashes += " " + d.digest('hex')
         }
         console.log(sk_hashes)
         await instance.shareSecretPoints(sk_hashes, {"from": members[m].address})
       }

       // Let's fetch the vvecs ...
       const vvecs = []
       for(var i = 0; i < numMembers; i++) {
           var verificationVector = []
           for(var j = 0; j < threshold; j++) {
               verificationVector.push((await instance.memberVerificationVectors(i,j)).toNumber())
           }
           vvecs.push(verificationVector)
       }

       //
       console.log("Now let's verify what we received...")
       for(var mx = 0; mx < numMembers; mx++) {
         for(var mxx = 0; mxx < numMembers; mxx++) {
             const sk = members[mx].receivedShares[mxx]
             if (!verifyContributionShare(bls, members[mx].point, sk, vvecs[mxx])) {
               await instance.protestSecretPoints(sk.toString(), mxx, { "from": members[mx].address })
               throw new Error('invalid share!')
             }
         }
         await instance.approveSecretPoints({ "from": members[mx].address })
       }


       members.forEach((member, i) => {
         const first = member.receivedShares.pop()
         member.receivedShares.forEach(sk => {
           bls.secretKeyAdd(first, sk) // Fr
           bls.free(sk)
         })
         member.secretKeyShare = first
       })
       console.log('secret shares have been generated...')

       const groupsVvec = []
       vvecs.forEach(vvec => {
         vvec.forEach((pk2, i) => {
           let pk1 = groupsVvec[i]
           if (!pk1) {
             groupsVvec[i] = pk2
           } else {
             bls.publicKeyAdd(pk1, pk2) // G2
             bls.free(pk2)
           }
         })
       })
       console.log('verification vector computed...')

       const groupsPublicKey = groupsVvec[0]
       const pubArray = bls.publicKeyExport(groupsPublicKey)
       console.log('group public key : ', Buffer.from(pubArray).toString('hex'))

       console.log('testing signature...')
       const message = 'hello rust'
       const sigs = []
       const signersIds = []
       for (let i = 0; i < threshold; i++) { // now we can select any 4 members to sign on a message
         const sig = bls.signature()
         bls.sign(sig, members[i].secretKeyShare, message)
         sigs.push(sig)
         signersIds.push(members[i].point)
       }

       const groupsSig = bls.signature()
       bls.signatureRecover(groupsSig, sigs, signersIds)

       const sigArray = bls.signatureExport(groupsSig)
       const sigBuf = Buffer.from(sigArray)
       console.log('sigtest result: ', sigBuf.toString('hex'))
       var verified = bls.verify(groupsSig, groupsPublicKey, message)
       console.log('verified: ', Boolean(verified))

       bls.free(groupsSig)
       bls.freeArray(groupsVvec)
       members.forEach(m => {
         bls.free(m.secretKeyShare)
         bls.free(m.point)
       })

       // END! //
       process.exit(1)
     })
    .catch(function(e) {
      console.log("Something went wrong")
      console.log(e)
      process.exit(1)
    });
})
