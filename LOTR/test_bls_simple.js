#!/usr/bin/env node
const bls = require('bls-lib')
bls.onModuleInit(() => {
  bls.init()

  const sec = bls.secretKey()
  const pub = bls.publicKey()
  const sig = bls.signature()

  bls.secretKeySetByCSPRNG(sec)
  const msg = 'hello world'
  console.log("Signing message: "+msg)
  bls.sign(sig, sec, msg)

  bls.getPublicKey(pub, sec)

  const v = bls.verify(sig, pub, msg)
  if(v === true) console.log("Signature matches: " + sig)
  // v === true

  bls.free(sec)
  bls.free(sig)
  bls.free(pub)
})
