pragma solidity 0.4.24;

contract Lotr {
  // contract versioning
  uint public version;

  // contract configuration - provided in the constructor
  uint public numMembers;
  uint public threshold;

  // evaluation points of the polynomial - randomly generated by the contract (cardinality: N)
  // TODO: consider encrypting them so that only the relevant member knows it (think if it's necessary, think if it's possible)
  uint[] public polynomialEvalPoints;

  // signatories of the group signature - addresses of transaction senders (cardinality: N)
  address[] public memberAddresses;

  // verification vectors - submitted by the signatories (cardinality: N * T)
  uint[][] public memberVerificationVectors;

  // * secret points shared between the participants, encrypted with the recipient's public key
  // * first dimension is the index of the sender, second dimension is the index of the recipient
  // * in each case the encrypted secret chunk is their encrypted polynomial evaluated at evaluation
  //   point corresponding to the recipient's index
  string[] public sharedSecretPoints;
  uint numSharedSecretPoints = 0;

  // whether the member approved all the secrets their received
  bool[] public memberApproved;

  // if a member decides to protest a secret point they received,
  // they can store this information here
  struct Protest {
    uint protestingMemberID;
    uint receivedSecretPoint;
    uint protestedMemberID;
    bool protested;
  }
  Protest public memberProtested;

	// whether all members approved the information they received from others
	bool public schemeApproved = false;

  // initialize the contract
  constructor(
    uint _numMembers,
    uint _threshold
  ) public {
    // versioning
    version = 1;

    // contract configuration
    numMembers = _numMembers;
    threshold = _threshold;

    // initialization of polynomial evaluation points
    for (uint i=0; i<numMembers; i++) {
      polynomialEvalPoints.push(i*7);
    }

   // initialisation of all data structures populated by members, but
   // not in their order of joining
   for (i=0; i<numMembers; i++) {
      sharedSecretPoints.push("");
      memberApproved.push(false);
    }
  }

  /**
   * To join the signature, the party needs to submit a public verification
   * polynomial function.
   *
   * The contract will record the polynomial and the address of the sender.
   *
   * @param _verificationVector public form of the signatory's polynomial function
   */
  function joinGroupSignature(
    uint[] _verificationVector
  ) public {
    // the verification vector represents a modified version of the
    // member's polynomial, so needs to have the appropriate cardinality
    require(_verificationVector.length == threshold);

    // there needs to be space for at least one more member
    require(memberAddresses.length < numMembers);

    memberAddresses.push(msg.sender);
    memberVerificationVectors.push(_verificationVector);
  }

  function shareSecretPoints(
    string _sharedSecretPoints
  ) public {
    // only a group member can share secret points
    GroupMembership memory groupMembership = getGroupMembership(msg.sender);
    require(groupMembership.isMember);

    // cannot share secret points twice
    require(bytes(sharedSecretPoints[groupMembership.memberID]).length == 0);

    sharedSecretPoints[groupMembership.memberID] = _sharedSecretPoints;
    numSharedSecretPoints += 1;
  }

  struct GroupMembership {
    bool isMember;
    uint memberID;
  }

  function getGroupMembership(address _address) private view returns (GroupMembership groupMembership) {
    bool isMember = false;
    uint memberID = 0;
    for (uint i=0; i<memberAddresses.length; i++) {
      if (memberAddresses[i] == _address) {
        isMember = true;
        memberID = i;
        break;
      }
    }
    return GroupMembership({
      isMember:isMember,
      memberID:memberID
    });
  }

  function getMemberID() public view returns (uint memberID) {
    GroupMembership memory groupMembership = getGroupMembership(msg.sender);

    require(groupMembership.isMember);

    return groupMembership.memberID;
  }

  function approveSecretPoints() public {
    // one can only review secret points if all of them have been shared
    require(numSharedSecretPoints == numMembers);

    // only a member of the signature scheme can review points
    GroupMembership memory senderGroupMembership = getGroupMembership(msg.sender);
    require(senderGroupMembership.isMember);

    // if the member has already protested, they cannot accept now
    if (memberProtested.protested) {
      require(memberProtested.protestingMemberID != senderGroupMembership.memberID);
    }

    memberApproved[senderGroupMembership.memberID] = true;
  }

  function protestSecretPoints(
    uint _receivedSecretPoint,
    uint _protestedMemberID
  ) public {
    // one can only review secret points if all of them have been shared
    require(sharedSecretPoints.length == numMembers);

    // only a member of the signature scheme can review points
    GroupMembership memory senderGroupMembership = getGroupMembership(msg.sender);
    require(senderGroupMembership.isMember);

    // if the member has already accepted, they cannot protest now
    require(memberApproved[senderGroupMembership.memberID] == false);

    // a member cannot protest themselves
    require(_protestedMemberID != senderGroupMembership.memberID);

    memberProtested = Protest({
      protestingMemberID:senderGroupMembership.memberID,
      receivedSecretPoint:_receivedSecretPoint,
      protestedMemberID:_protestedMemberID,
      protested:true
    });
  }

  /**
   * Finalizes the signature scheme, by performing all necessary checks
   * and updating the contract state to expose the public key.
   */
  function finalizeScheme() public {
    require(memberAddresses.length == numMembers);
    assert(memberVerificationVectors.length == numMembers);
    for (uint i=0; i<numMembers; i++) {
      require(memberApproved[i]);
    }

    schemeApproved = true;
  }
}
