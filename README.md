Lord of the rings
=============
**Step 0. Build stuff**
```bash
docker run -d -p 8545:8545 --name ganache trufflesuite/ganache-cli:v6.1.0 -m "shoot maximum math license order evil game hire foot elbow bicycle add"
docker run -i --link ganache:node -v $(pwd):/usr/src/app -w /usr/src/app nenv /bin/sh -c "env" # check if IP matches truffle.js config

docker build . -t nenv
docker run -i -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "cd LOTR; ./test_bls_simple.js"
docker run -i -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "cd LOTR; ./test_bls_threshold.js"
docker run -i -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "cd LOTR; yarn install"

git clone git://github.com/herumi/xbyak.git
git clone git://github.com/herumi/cybozulib.git
git clone git://github.com/herumi/mcl.git
git clone git://github.com/herumi/bls.git
docker run -i -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "cd bls; make test UNIT=4"
```

**Step 1. Deploy and run smart-contracts**

With Ganache:
```bash
docker run -i --net=host -v $(pwd):/usr/src/app -w /usr/src/app nenv /bin/sh -c "cd LOTR; yarn migrate; ./main.js"
```

With Rinkeby (uncommenting the Infura provider beforehand):
```bash
docker run -i -v $(pwd):/usr/src/app -w /usr/src/app nenv /bin/sh -c "cd LOTR; yarn deploy; ./main.js"
```


> *Handy aliases:*  
> d: docker run -i --link ganache:node -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "$@"  
> d-net: docker run -i --net=host -v `pwd`:/usr/src/app -w /usr/src/app nenv /bin/sh -c "$@"  
>
> *To examine the smart-contract:*
> ``d "cd LOTR; truffle console --network rinkeby"``
> ```javascript
> Lotr.deployed().then(instance => { lt = instance })
> lt.version.call()
> ```

